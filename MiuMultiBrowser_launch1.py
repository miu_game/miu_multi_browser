import time

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtWebEngineWidgets import *

import sys
from miu_browser import MyWebEngineView


from PyQt5.QtCore import QRunnable, Qt, QThreadPool
from PyQt5.QtWidgets import (
    QApplication,
    QLabel,
    QMainWindow,
    QPushButton,
    QVBoxLayout,
    QWidget,
)
import logging

# 1. Subclass QRunnable
class Runnable(QRunnable):
    def __init__(self,role_utils):
        super().__init__()
        self.role_utils = role_utils
    def run(self):
        # Your long-running task goes here ...
        items = self.role_utils.get_items()
        print('QRunnable run')
        # for item in items:
        #     print(item)
        for item in items:
            str_desc = item[6]
            item_name = item[0]
            item_id = item[2]
            item_count = item[1]
            # print(item)
            if 'arm.armor' in str_desc and 'uncommon' in str_desc:
                print(item)
                if False:
                    self.role_utils.send_command('/foo fenjie {}'.format(item_id))
                    time.sleep(7)
        pass


class MainWindow(QWidget):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        if False:
            layout = QGridLayout()
            # Add widgets to the layout
            layout.addWidget(QPushButton("Button at (0, 0)"), 0, 0)
            layout.addWidget(QPushButton("Button at (0, 1)"), 0, 1)
            layout.addWidget(QPushButton("Button Spans two Cols"), 1, 0, 1, 2)
            # Set the layout on the application's window
            self.setLayout(layout)
            self.show()

    def setup(self, username, password):
        self.setWindowTitle('miu')
        import utils
        from miu_browser import MyWebEngineView1
        m_89, jsessionid, role_id_list = utils.get_m_jsessionid_id_list(username, password)

        role_info_list = []

        self.browser = MyWebEngineView1()
        self.browser.setup(m_89, jsessionid, role_id_list[0])
        role_info_list.append({
            'm_89': m_89,
            'jsessionid': jsessionid,
            'role_id': role_id_list[0]
        })

        m_89, jsessionid, role_id_list = utils.get_m_jsessionid_id_list(username, password)
        self.browser1 = MyWebEngineView1()
        self.browser1.setup(m_89, jsessionid, role_id_list[1])
        role_info_list.append({
            'm_89': m_89,
            'jsessionid': jsessionid,
            'role_id': role_id_list[1]
        })

        m_89, jsessionid, role_id_list = utils.get_m_jsessionid_id_list(username, password)
        self.browser2 = MyWebEngineView1()
        self.browser2.setup(m_89, jsessionid, role_id_list[2])
        role_info_list.append({
            'm_89': m_89,
            'jsessionid': jsessionid,
            'role_id': role_id_list[2]
        })

        m_89, jsessionid, role_id_list = utils.get_m_jsessionid_id_list(username, password)
        self.browser3 = MyWebEngineView1()
        self.browser3.setup(m_89, jsessionid, role_id_list[3])
        role_info_list.append({
            'm_89': m_89,
            'jsessionid': jsessionid,
            'role_id': role_id_list[3]
        })

        m_89, jsessionid, role_id_list = utils.get_m_jsessionid_id_list(username, password)
        self.browser4 = MyWebEngineView1()
        self.browser4.setup(m_89, jsessionid, role_id_list[4])
        role_info_list.append({
            'm_89': m_89,
            'jsessionid': jsessionid,
            'role_id': role_id_list[4]
        })

        layout = QGridLayout()
        layout.addWidget(self.browser, 0, 0)
        layout.addWidget(self.browser1, 0, 1)

        layout.addWidget(self.browser2, 0, 2)
        layout.addWidget(self.browser3, 1, 0)
        layout.addWidget(self.browser4, 1, 1)
        self.setLayout(layout)

        # setup buttons
        if True:
            button_grid = QGridLayout()
            layout.addLayout(button_grid, 1, 2)
            self.teamButton = QPushButton('组队（会先退出当前队伍')
            button_grid.addWidget(self.teamButton, 0, 0)

            self.followButton = QPushButton('跟随')
            button_grid.addWidget(self.followButton, 1, 0)

            self.repairButton = QPushButton('修理全部')
            button_grid.addWidget(self.repairButton, 2, 0)

            # self. = QLineEdit()
            # button_grid.addWidget(self.clearBagButton, 3, 0)
            self.clearBagButton = QPushButton('丢弃绿图/溶xx石/')
            button_grid.addWidget(self.clearBagButton, 4, 0)

            self.learnButton = QPushButton('学习分解（需要先走到哨所')
            button_grid.addWidget(self.learnButton, 5, 0)

            # self.learnButton = QPushButton('学习分解（需要先走到哨所')
            # button_grid.addWidget(self.learnButton, 6, 0)

            self.disenchantButton = QPushButton('分解所有绿装')
            button_grid.addWidget(self.disenchantButton, 7, 0)

            # 分解添加到快捷栏
            #     /putcut skill skill.output.FenJie 11
            # /foo fenjie 109068010

            r0 = role_info_list[0]
            r1 = role_info_list[1]
            r2 = role_info_list[2]
            r3 = role_info_list[3]
            r4 = role_info_list[4]
            m0, j0, i0 = r0.values()
            m1, j1, i1 = r1.values()
            m2, j2, i2 = r2.values()
            m3, j3, i3 = r3.values()
            m4, j4, i4 = r4.values()
            SLEEP_INTERVAL = 1

            def teamButtonClicked():
                r0 = role_info_list[0]
                r1 = role_info_list[1]
                r2 = role_info_list[2]
                r3 = role_info_list[3]
                r4 = role_info_list[4]
                m0, j0, i0 = r0.values()
                m1, j1, i1 = r1.values()
                m2, j2, i2 = r2.values()
                m3, j3, i3 = r3.values()
                m4, j4, i4 = r4.values()
                role_utils = utils.setup_role_utils(m0, j0)
                SLEEP_INTERVAL = 0.3

                role_utils = utils.setup_role_utils(m0, j0)
                role_utils.send_command("/foo rank tui {}".format(i0))
                time.sleep(SLEEP_INTERVAL)
                role_utils = utils.setup_role_utils(m1, j1)
                role_utils.send_command("/foo rank tui {}".format(i0))
                time.sleep(SLEEP_INTERVAL)
                role_utils = utils.setup_role_utils(m2, j2)
                role_utils.send_command("/foo rank tui {}".format(i0))
                time.sleep(SLEEP_INTERVAL)
                role_utils = utils.setup_role_utils(m3, j3)
                role_utils.send_command("/foo rank tui {}".format(i0))
                time.sleep(SLEEP_INTERVAL)
                role_utils = utils.setup_role_utils(m4, j4)
                role_utils.send_command("/foo rank tui {}".format(i0))
                time.sleep(SLEEP_INTERVAL)

                role_utils = utils.setup_role_utils(m0, j0)
                role_utils.send_command("/foo rank add {}".format(i1))
                time.sleep(SLEEP_INTERVAL)
                role_utils.send_command("/foo rank add {}".format(i2))
                time.sleep(SLEEP_INTERVAL)
                role_utils.send_command("/foo rank add {}".format(i3))
                time.sleep(SLEEP_INTERVAL)
                role_utils.send_command("/foo rank add {}".format(i4))
                time.sleep(SLEEP_INTERVAL)

                role_utils = utils.setup_role_utils(m1, j1)
                role_utils.send_command("/foo rank agree {}".format(i0))
                time.sleep(SLEEP_INTERVAL)
                role_utils = utils.setup_role_utils(m2, j2)
                role_utils.send_command("/foo rank agree {}".format(i0))
                time.sleep(SLEEP_INTERVAL)
                role_utils = utils.setup_role_utils(m3, j3)
                role_utils.send_command("/foo rank agree {}".format(i0))
                time.sleep(SLEEP_INTERVAL)
                role_utils = utils.setup_role_utils(m4, j4)
                role_utils.send_command("/foo rank agree {}".format(i0))
                time.sleep(SLEEP_INTERVAL)

            def follow():
                r0 = role_info_list[0]
                r1 = role_info_list[1]
                r2 = role_info_list[2]
                r3 = role_info_list[3]
                r4 = role_info_list[4]
                m0, j0, i0 = r0.values()
                m1, j1, i1 = r1.values()
                m2, j2, i2 = r2.values()
                m3, j3, i3 = r3.values()
                m4, j4, i4 = r4.values()
                SLEEP_INTERVAL = 1

                role_utils = utils.setup_role_utils(m1, j1)
                role_utils.send_command("/follow {}".format(i0))
                time.sleep(SLEEP_INTERVAL)
                role_utils = utils.setup_role_utils(m2, j2)
                role_utils.send_command("/follow {}".format(i0))
                time.sleep(SLEEP_INTERVAL)
                role_utils = utils.setup_role_utils(m3, j3)
                role_utils.send_command("/follow {}".format(i0))
                time.sleep(SLEEP_INTERVAL)
                role_utils = utils.setup_role_utils(m4, j4)
                role_utils.send_command("/follow {}".format(i0))
                time.sleep(SLEEP_INTERVAL)

            def repair():
                r0 = role_info_list[0]
                r1 = role_info_list[1]
                r2 = role_info_list[2]
                r3 = role_info_list[3]
                r4 = role_info_list[4]
                m0, j0, i0 = r0.values()
                m1, j1, i1 = r1.values()
                m2, j2, i2 = r2.values()
                m3, j3, i3 = r3.values()
                m4, j4, i4 = r4.values()
                SLEEP_INTERVAL = 1

                role_utils = utils.setup_role_utils(m0, j0)
                role_utils.send_command("/xiuli all".format(i0))
                time.sleep(SLEEP_INTERVAL)
                role_utils = utils.setup_role_utils(m1, j1)
                role_utils.send_command("/xiuli all".format(i0))
                time.sleep(SLEEP_INTERVAL)
                role_utils = utils.setup_role_utils(m2, j2)
                role_utils.send_command("/xiuli all".format(i0))
                time.sleep(SLEEP_INTERVAL)
                role_utils = utils.setup_role_utils(m3, j3)
                role_utils.send_command("/xiuli all".format(i0))
                time.sleep(SLEEP_INTERVAL)
                role_utils = utils.setup_role_utils(m4, j4)
                role_utils.send_command("/xiuli all".format(i0))
                time.sleep(SLEEP_INTERVAL)

            def clearBag():

                role_utils = utils.setup_role_utils(m0, j0)
                items = role_utils.get_items()
                for item in items:
                    print(item)
                    # if item[0] in []:
                    #     role_utils.send_command('/drop {} {}'.format(item[2],item[1]))

                role_utils_list = [utils.setup_role_utils(m0, j0),
                                   utils.setup_role_utils(m1, j1),
                                   utils.setup_role_utils(m2, j2),
                                   utils.setup_role_utils(m3, j3),
                                   utils.setup_role_utils(m4, j4)
                                   ]
                for role_utils in role_utils_list:
                    items = role_utils.get_items()
                    time.sleep(1)
                    for item in items:
                        # print(item)
                        str_desc = item[6]
                        item_name = item[0]
                        item_id = item[2]
                        item_count = item[1]
                        if item_name.startswith('魔石图') and not item_name.endswith('晶石') and not item_name.endswith(
                                '魔石'):
                            print(item_name, str_desc)
                            role_utils.send_command('/drop {} {}'.format(item_id, item_count))
                            time.sleep(1)
                        if item_name.startswith('设计图') and 'lv' in str_desc.lower():
                            print(item_name, str_desc)
                            role_utils.send_command('/drop {} {}'.format(item_id, item_count))
                            time.sleep(1)
                        if item_name.startswith('缝纫图') and 'lv' in str_desc.lower():
                            print(item_name, str_desc)
                            role_utils.send_command('/drop {} {}'.format(item_id, item_count))
                            time.sleep(1)
                        if '溶' in item_name and item_name.endswith('石'):
                            print(item_name, str_desc)
                            role_utils.send_command('/drop {} {}'.format(item_id, item_count))
                            time.sleep(1)

                        # if (item_name.startswith('缝纫图')
                        #     or item_name.startswith('魔石图')
                        #     or item_name.startswith('设计图')
                        # ):
                        # if (item_name.startswith('缝纫图')
                        #     or item_name.startswith('魔石图')
                        #     or item_name.startswith('设计图')
                        # ) and ('uncommon' in str_desc.lower or 'lv' in str_desc.lower):
                        #     role_utils.send_command('/drop {} {}'.format(item_id, item_count))
                        #     time.sleep(1)

        def learn_disenchant():
            r0 = role_info_list[0]
            r1 = role_info_list[1]
            r2 = role_info_list[2]
            r3 = role_info_list[3]
            r4 = role_info_list[4]
            m0, j0, i0 = r0.values()
            m1, j1, i1 = r1.values()
            m2, j2, i2 = r2.values()
            m3, j3, i3 = r3.values()
            m4, j4, i4 = r4.values()
            SLEEP_INTERVAL = 1


            role_utils = utils.setup_role_utils(m0, j0)
            role_utils.send_command("/talk 1 z_2")

            time.sleep(SLEEP_INTERVAL)
            role_utils = utils.setup_role_utils(m1, j1)
            role_utils.send_command("/talk 1 z_2")

            time.sleep(SLEEP_INTERVAL)
            role_utils = utils.setup_role_utils(m2, j2)
            role_utils.send_command("/talk 1 z_2")

            time.sleep(SLEEP_INTERVAL)
            role_utils = utils.setup_role_utils(m3, j3)
            role_utils.send_command("/talk 1 z_2")

            time.sleep(SLEEP_INTERVAL)
            role_utils = utils.setup_role_utils(m4, j4)
            role_utils.send_command("/talk 1 z_2")

        def disenchant():
            r0 = role_info_list[0]
            r1 = role_info_list[1]
            r2 = role_info_list[2]
            r3 = role_info_list[3]
            r4 = role_info_list[4]
            m0, j0, i0 = r0.values()
            m1, j1, i1 = r1.values()
            m2, j2, i2 = r2.values()
            m3, j3, i3 = r3.values()
            m4, j4, i4 = r4.values()
            SLEEP_INTERVAL = 1
            print('disenchant')

            role_utils = utils.setup_role_utils(m0, j0)
            role_utils_list = [utils.setup_role_utils(m0, j0),
                               utils.setup_role_utils(m1, j1),
                               utils.setup_role_utils(m2, j2),
                               utils.setup_role_utils(m3, j3),
                               utils.setup_role_utils(m4, j4)
                               ]
            pool = QThreadPool.globalInstance()
            for role_utils in role_utils_list:
                # threadCount = QThreadPool.globalInstance().maxThreadCount()
                runnable = Runnable(role_utils)
                pool.start(runnable)
                continue
                items = role_utils.get_items()
                time.sleep(1)
                for item in items:
                    str_desc = item[6]
                    item_name = item[0]
                    item_id = item[2]
                    item_count = item[1]
                    # print(item)
                    if 'arm.armor' in str_desc and 'uncommon' in str_desc:
                        print(item)
                        # role_utils.send_command('/')

                #     if item[0].startswith('缝纫图'):

            #
            # time.sleep(SLEEP_INTERVAL)
            # role_utils = utils.setup_role_utils(m1, j1)
            #
            # time.sleep(SLEEP_INTERVAL)
            # role_utils = utils.setup_role_utils(m2, j2)
            #
            # time.sleep(SLEEP_INTERVAL)
            # role_utils = utils.setup_role_utils(m3, j3)
            #
            # time.sleep(SLEEP_INTERVAL)
            # role_utils = utils.setup_role_utils(m4, j4)

        self.teamButton.clicked.connect(teamButtonClicked)
        self.followButton.clicked.connect(follow)
        self.repairButton.clicked.connect(repair)
        self.clearBagButton.clicked.connect(clearBag)
        self.learnButton.clicked.connect(learn_disenchant)
        self.disenchantButton.clicked.connect(disenchant)

        print(role_info_list)

        # self.setCentralWidget(self.browser)
        self.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)

    g_u = None
    g_p = None
    g_window = None


    class LoginWin(QMainWindow):

        def __init__(self):
            super(LoginWin, self).__init__()

            self.setWindowTitle("My App")

            layout = QGridLayout()
            uInput = QLineEdit('ceshizhanghao000')
            pInput = QLineEdit('ceshizhanghao')
            loginButton = QPushButton('多开')
            layout.addWidget(uInput, 0, 0)
            layout.addWidget(pInput, 1, 0)
            layout.addWidget(loginButton, 2, 0)
            widget = QWidget()
            widget.setLayout(layout)
            self.setCentralWidget(widget)

            def login():
                g_u = uInput.text()
                g_p = pInput.text()

                self.close()
                if True:
                    g_window.setup(g_u, g_p)

            loginButton.clicked.connect(login)



    lw = LoginWin()
    lw.show()

    g_window = MainWindow()

    app.exec_()
