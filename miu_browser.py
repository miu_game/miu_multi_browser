#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on 2022/05/12
@author: Irony
@site: https://pyqt.site https://github.com/PyQt5
@email: 892768447@qq.com
@file: SetCookies.py
@description: 主动设置Cookie
"""

from PyQt5.QtCore import QDateTime, Qt, QUrl
from PyQt5.QtWebEngineWidgets import QWebEngineProfile, QWebEngineView, QWebEnginePage
from PyQt5.QtWidgets import QApplication
from PyQt5.QtNetwork import QNetworkCookie
from PyQt5 import QtGui


class MyWebEngineView(QWebEngineView):

    def __init__(self, *args, **kwargs):
        super(MyWebEngineView, self).__init__(*args, **kwargs)
        # global
        # self.cookieStore = QWebEngineProfile.defaultProfile().cookieStore()


    def setup(self,username,password,role_index):

        print(role_index)
        import utils

        m_89, jsessionid, role_id_list = utils.get_m_jsessionid_id_list(username, password)

        self.m_89 = m_89
        self.jsessionid = jsessionid

        role_id = role_id_list[role_index]
        print(m_89,jsessionid,role_id)

        client_html = utils.login_role(m_89, jsessionid, role_id)
        role_utils = utils.setup_role_utils(m_89, jsessionid)
        validateParam, current_room = utils.get_login_info_from_client_html(client_html)


        if True:
            storage = "./miu_profile/" + str(role_id)
            profile = QWebEngineProfile(storage, self)
            page = QWebEnginePage(profile, self)
            # self.setPage(page)
            self.setPage(page)

        # current
        self.cookieStore = self.page().profile().cookieStore()
        # self.cookieStore = QWebEngineProfile.defaultProfile().cookieStore()
        # self.initCookies()





        self.cookies = [{
            "domain": "51wan.olplay.com:89",
            "hostOnly": True,
            "httpOnly": False,
            "name": "JSESSIONID",
            "value": jsessionid,
            "path": "/",
            "secure": False,
        }, {
            "domain": "51wan.olplay.com:89",
            "hostOnly": True,
            "httpOnly": False,
            "name": "m",
            "value": m_89,
            "path": "/",
            "secure": False,
        }]

        for cookie in self.cookies:
            qcookie = QNetworkCookie()
            qcookie.setName(cookie.get('name', '').encode())
            print(cookie.get('value', ''))
            qcookie.setValue(cookie.get('value', '').encode())
            qcookie.setDomain(cookie.get('domain', ''))
            qcookie.setPath(cookie.get('path', ''))
            qcookie.setExpirationDate(
                QDateTime.fromString(str(cookie.get('expirationDate', 0)),
                                     Qt.ISODate))
            qcookie.setHttpOnly(cookie.get('httpOnly', False))
            qcookie.setSecure(cookie.get('secure', False))
            # 注意可以设置具体的url
            self.cookieStore.setCookie(qcookie, QUrl())

        self.loadProgress.connect(self.onLoadProgress)
        self.load(QUrl('http://51wan.olplay.com:89/'))

    def onLoadProgress(self, progress):
        print(progress)
        if progress == 80:
            # print(self.m_89,self.jsessionid)
            print(self.cookieStore)
            # self.page().runJavaScript('alert(document.cookie);')
            # self.page().runJavaScript('alert(document);')
            # self.webEngineView.page().setDevToolsPage(self.inspector.page())
            # self.page().setDevToolsPage(self.page())

            # self.page().runJavaScript('alert(document.cookie);')
            # self.page().runJavaScript('document.write(document.cookie);')

            if True:
                if False:
                    DEBUG_PORT = '5588'
                    DEBUG_URL = 'http://127.0.0.1:%s' % DEBUG_PORT
                    import os
                    os.environ['QTWEBENGINE_REMOTE_DEBUGGING'] = DEBUG_PORT
                    self.inspector = QWebEngineView()
                    self.inspector.setMyWebEngineViewTitle('Web Inspector')
                    self.inspector.load(QUrl(DEBUG_URL))
                    # self.page().loadFinished.connect(self.handleHtmlLoaded)
                    self.page().setDevToolsPage(self.inspector.page())
                    self.inspector.show()
                    # document.getElementsByTagName('frame')[0].contentMyWebEngineView.document.getElementById('gameDoc')

                if True:
                    # self.page().runJavaScript("console.log(document.querySelector('#gameDoc'))")
                    self.page().runJavaScript("""
                   let count = 0;
                let timer = setInterval(()=>{
                    let dcmt = window.top.document.querySelector("frame").contentDocument
                   dcmt.querySelector('#gameDoc')       
                   dcmt.body.style.zoom = '80%'
                   
                   var inputBar = dcmt.querySelector('.inputBar')
                   inputBar.style.marginLeft = '-800px'
                   inputBar.style.marginTop = '50px'
                   
                   var inputBar  = dcmt.querySelector('#sd_meet_gen')
                   inputBar.style.marginLeft = '-600px'
                   inputBar.style.marginTop = '50px'
                   
                   var inputBar = dcmt.querySelector('#opt_meet_gen')
                   inputBar.style.marginLeft = '-600px'
                   inputBar.style.marginTop = '50px'
                   
                   if(count >5){
                        clearInterval(timer)
                   }
                    count += 1;
                    //dcmt.querySelector('#chatReader').remove()
                    //dcmt.querySelector('#chatMyReader').remove()
                    //dcmt.querySelector('#helpBar').remove()
                },3000)                     
                
                   
                    """)

        # if progress == 100:
        #     # 测试获取cookie
        #     print('progress 100')
        #     self.page().runJavaScript('alert(document.cookie);')


class MyWebEngineView1(QWebEngineView):

    def __init__(self, *args, **kwargs):
        super(MyWebEngineView1, self).__init__(*args, **kwargs)
        # global
        # self.cookieStore = QWebEngineProfile.defaultProfile().cookieStore()

    def setup(self, m_89, jsessionid, role_id):

        # # print(role_index)
        import utils
        #
        # m_89, jsessionid, role_id_list = utils.get_m_jsessionid_id_list(username, password)
        #
        # self.m_89 = m_89
        # self.jsessionid = jsessionid
        #
        # role_id = role_id_list[role_index]
        # print(m_89, jsessionid, role_id)

        client_html = utils.login_role(m_89, jsessionid, role_id)
        role_utils = utils.setup_role_utils(m_89, jsessionid)
        validateParam, current_room = utils.get_login_info_from_client_html(client_html)

        if True:
            storage = "./miu_profile/" + str(role_id)
            profile = QWebEngineProfile(storage, self)
            page = QWebEnginePage(profile, self)
            # self.setPage(page)
            self.setPage(page)

        # current
        self.cookieStore = self.page().profile().cookieStore()
        # self.cookieStore = QWebEngineProfile.defaultProfile().cookieStore()
        # self.initCookies()


        self.cookies = [{
            "domain": "51wan.olplay.com:89",
            "hostOnly": True,
            "httpOnly": False,
            "name": "JSESSIONID",
            "value": jsessionid,
            "path": "/",
            "secure": False,
        }, {
            "domain": "51wan.olplay.com:89",
            "hostOnly": True,
            "httpOnly": False,
            "name": "m",
            "value": m_89,
            "path": "/",
            "secure": False,
        }]

        for cookie in self.cookies:
            qcookie = QNetworkCookie()
            qcookie.setName(cookie.get('name', '').encode())
            print(cookie.get('value', ''))
            qcookie.setValue(cookie.get('value', '').encode())
            qcookie.setDomain(cookie.get('domain', ''))
            qcookie.setPath(cookie.get('path', ''))
            qcookie.setExpirationDate(
                QDateTime.fromString(str(cookie.get('expirationDate', 0)),
                                     Qt.ISODate))
            qcookie.setHttpOnly(cookie.get('httpOnly', False))
            qcookie.setSecure(cookie.get('secure', False))
            # 注意可以设置具体的url
            self.cookieStore.setCookie(qcookie, QUrl())

        self.loadProgress.connect(self.onLoadProgress)
        self.load(QUrl('http://51wan.olplay.com:89/'))

    def onLoadProgress(self, progress):
        print(progress)
        if progress == 80:
            # print(self.m_89,self.jsessionid)
            print(self.cookieStore)
            # self.page().runJavaScript('alert(document.cookie);')
            # self.page().runJavaScript('alert(document);')
            # self.webEngineView.page().setDevToolsPage(self.inspector.page())
            # self.page().setDevToolsPage(self.page())

            # self.page().runJavaScript('alert(document.cookie);')
            # self.page().runJavaScript('document.write(document.cookie);')

            if True:
                if False:
                    DEBUG_PORT = '5588'
                    DEBUG_URL = 'http://127.0.0.1:%s' % DEBUG_PORT
                    import os
                    os.environ['QTWEBENGINE_REMOTE_DEBUGGING'] = DEBUG_PORT
                    self.inspector = QWebEngineView()
                    self.inspector.setMyWebEngineViewTitle('Web Inspector')
                    self.inspector.load(QUrl(DEBUG_URL))
                    # self.page().loadFinished.connect(self.handleHtmlLoaded)
                    self.page().setDevToolsPage(self.inspector.page())
                    self.inspector.show()
                    # document.getElementsByTagName('frame')[0].contentMyWebEngineView.document.getElementById('gameDoc')

                if True:
                    # self.page().runJavaScript("console.log(document.querySelector('#gameDoc'))")
                    self.page().runJavaScript("""
                   let count = 0;
                let timer = setInterval(()=>{
                    let dcmt = window.top.document.querySelector("frame").contentDocument
                    
                   dcmt.querySelector('#gameDoc')       
                   dcmt.body.style.zoom = '70%'

                   var inputBar = dcmt.querySelector('.inputBar')
                   inputBar.style.marginLeft = '-800px'
                   inputBar.style.marginTop = '50px'

                   var inputBar  = dcmt.querySelector('#sd_meet_gen')
                   inputBar.style.marginLeft = '-600px'
                   inputBar.style.marginTop = '50px'

                   var inputBar = dcmt.querySelector('#opt_meet_gen')
                   inputBar.style.marginLeft = '-600px'
                   inputBar.style.marginTop = '50px'

                   if(count >5){
                        clearInterval(timer)
                   }
                    count += 1;
                    //dcmt.querySelector('#chatReader').remove()
                    //dcmt.querySelector('#chatMyReader').remove()
                    //dcmt.querySelector('#helpBar').remove()
                },3000)                     


                    """)

        # if progress == 100:
        #     # 测试获取cookie
        #     print('progress 100')
        #     self.page().runJavaScript('alert(document.cookie);')


if __name__ == '__main__':
    import cgitb
    import sys

    cgitb.enable(format='text')
    app = QApplication(sys.argv)
    w = MyWebEngineView()
    w.show()
    sys.exit(app.exec_())
